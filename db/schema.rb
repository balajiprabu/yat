# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170202153754) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contracts", force: :cascade do |t|
    t.string   "contract_no",                       null: false
    t.boolean  "sequence_tested",   default: false
    t.boolean  "end_to_end_tested", default: false
    t.text     "dm_file_path"
    t.text     "csf_file_path"
    t.string   "scenario",          default: ""
    t.datetime "allocated_at"
    t.string   "dm_page_range"
    t.integer  "user_id"
    t.integer  "contract_weight",   default: 0
    t.integer  "product_id"
  end

  add_index "contracts", ["contract_no", "scenario", "product_id"], name: "contract_table_unique_index", unique: true, using: :btree
  add_index "contracts", ["product_id"], name: "index_contracts_on_product_id", using: :btree
  add_index "contracts", ["user_id"], name: "index_contracts_on_user_id", using: :btree

  create_table "contracts_forms", id: false, force: :cascade do |t|
    t.integer "contract_id"
    t.integer "form_id"
  end

  add_index "contracts_forms", ["contract_id"], name: "index_contracts_forms_on_contract_id", using: :btree
  add_index "contracts_forms", ["form_id", "contract_id"], name: "index_contracts_forms_on_form_id_and_contract_id", unique: true, using: :btree
  add_index "contracts_forms", ["form_id"], name: "index_contracts_forms_on_form_id", using: :btree

  create_table "forms", force: :cascade do |t|
    t.string   "form_name",                                     null: false
    t.datetime "actual_drop_date"
    t.boolean  "story_card_validated", default: false
    t.boolean  "released",             default: false
    t.string   "executed_with",        default: ""
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "form_status",          default: "Untriggered"
    t.string   "form_exec_status",     default: "Yet to start"
    t.text     "comments"
    t.datetime "executed_at"
    t.integer  "product_id"
  end

  add_index "forms", ["form_name", "product_id"], name: "index_forms_on_form_name_and_product_id", unique: true, using: :btree
  add_index "forms", ["product_id"], name: "index_forms_on_product_id", using: :btree

  create_table "mocktests", force: :cascade do |t|
    t.text     "dm_file_path"
    t.text     "csf_file_path"
    t.boolean  "form_tested"
    t.datetime "tested_at"
    t.datetime "allocated_at"
  end

  create_table "products", force: :cascade do |t|
    t.string   "product_name",                                      null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.date     "planned_drop_date"
    t.boolean  "mapped_or_product_with_zero_forms", default: false
    t.string   "lob",                               default: ""
    t.boolean  "rtc_task_updated",                  default: false
    t.integer  "release_id"
    t.string   "product_status",                    default: ""
  end

  add_index "products", ["product_name", "release_id"], name: "index_products_on_product_name_and_release_id", unique: true, using: :btree
  add_index "products", ["release_id"], name: "index_products_on_release_id", using: :btree

  create_table "releases", force: :cascade do |t|
    t.string   "name",       null: false
    t.boolean  "current"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "releases", ["name"], name: "index_releases_on_name", unique: true, using: :btree

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "name"
    t.string "password"
    t.string "group"
    t.string "hint"
  end

end
