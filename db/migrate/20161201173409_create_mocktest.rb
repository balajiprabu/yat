class CreateMocktest < ActiveRecord::Migration
  def change
    create_table :mocktests do |t|
      t.text :dm_file_path
      t.text :csf_file_path
      t.boolean :form_tested
      t.datetime :tested_at
      t.datetime :allocated_at
    end

    add_column :contracts, :sequence_tested_at, :datetime
    add_column :contracts, :end_to_end_tested_at, :datetime
    add_column :contracts, :scenario, :string
    add_column :contracts, :allocated_at, :datetime

    add_column :forms, :comments, :text
    rename_column :forms, :test_exec_status, :form_exec_status

  end
end
