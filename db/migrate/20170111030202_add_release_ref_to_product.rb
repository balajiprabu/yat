class AddReleaseRefToProduct < ActiveRecord::Migration
  def change
    remove_column :products, :release_id
    add_reference :products, :release, type: :string, index: true
  end
end
