class ChangeColumnName < ActiveRecord::Migration
  def change
    rename_column :products, :story_card_updated, :rtc_task_updated
  end
end
