class AddUniqueIndexAgain < ActiveRecord::Migration
  def change
    add_index :forms, [:form_name, :product_id], unique: true
    add_index :products, [:product_name,:release_id], unique: true
    remove_index :products, :product_name
  end
end
