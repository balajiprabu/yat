class AddFormsStatusToForms < ActiveRecord::Migration
  def change
    add_column :forms, :form_status, :string
    add_column :forms, :test_exec_status, :boolean
  end
end
