class AddColumnForms < ActiveRecord::Migration
  def change
    add_column :forms, :executed_at, :datetime
  end
end
