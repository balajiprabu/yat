class SetDefaultValues < ActiveRecord::Migration
  def change
    change_column_default :products, :lob, ""
    change_column_default :forms, :executed_with, ""
    change_column_default :contracts, :scenario, ""
    change_column_default :contracts, :contract_weight, 0
  end
end
