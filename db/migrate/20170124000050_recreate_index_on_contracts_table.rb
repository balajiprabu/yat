class RecreateIndexOnContractsTable < ActiveRecord::Migration
  def change
    remove_index :contracts, name: :index_contracts_on_contract_no_and_scenario
    add_index :contracts, [:contract_no, :scenario, :product_id], name: 'contract_table_unique_index', unique: true
  end
end
