class AddProductIdToForms < ActiveRecord::Migration
  def change
    add_reference :forms, :product, type: :string
    add_index :forms, [:form_name, :product_id], unique: true
    change_column_null :forms, :form_name, false
    change_column_default :forms, :released, false
    change_column_default :forms, :story_card_validated, false
    change_column_default :forms, :form_status, "Untriggered"
    change_column_default :forms, :form_exec_status, "Yet to start"
  end
end
