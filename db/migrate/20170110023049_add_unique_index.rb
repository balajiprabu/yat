class AddUniqueIndex < ActiveRecord::Migration
  def change
    change_column_null :releases, :name, false
    add_index :releases, :name, unique: true
    add_index :products, :product_name, unique: true
  end
end
