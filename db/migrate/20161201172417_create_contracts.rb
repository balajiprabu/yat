class CreateContracts < ActiveRecord::Migration
  def change
    create_table :contracts do |t|
      t.string :contract_no
      t.boolean :sequence_tested
      t.boolean :end_to_end_tested
      t.text :dm_file_path
      t.text :csf_file_path
    end
  end
end
