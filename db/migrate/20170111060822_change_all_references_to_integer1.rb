class ChangeAllReferencesToInteger1 < ActiveRecord::Migration
  def change
    remove_reference :forms, :product
    remove_reference :contracts, :user
    add_reference :products, :release, index: true
    add_reference :forms, :product, index: true
    add_reference :contracts, :user, index: true
  end
end
