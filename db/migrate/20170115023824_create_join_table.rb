class CreateJoinTable < ActiveRecord::Migration
  def change
    create_table :contracts_forms, id: false do |t|
      t.belongs_to :contract, index: true
      t.belongs_to :form, index: true
    end
  end
end
