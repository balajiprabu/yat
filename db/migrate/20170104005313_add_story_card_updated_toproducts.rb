class AddStoryCardUpdatedToproducts < ActiveRecord::Migration
  def change
    add_column :products, :story_card_updated, :boolean, default: false
    change_column_null :products, :product_name, false
  end
end
