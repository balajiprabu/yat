class AddLobToProduct < ActiveRecord::Migration
  def change
    add_column :products, :lob, :string
  end
end
