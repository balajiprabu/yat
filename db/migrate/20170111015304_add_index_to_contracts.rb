class AddIndexToContracts < ActiveRecord::Migration
  def change
    add_reference :contracts, :user, type: :string
    add_index :contracts, [:contract_no, :scenario], unique: true
    change_column_null :contracts, :contract_no, false
    change_column_default :contracts, :sequence_tested, false
    change_column_default :contracts, :end_to_end_tested, false

  end
end
