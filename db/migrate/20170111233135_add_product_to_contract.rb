class AddProductToContract < ActiveRecord::Migration
  def change
    add_column :contracts, :contract_weight, :integer
    add_reference :contracts, :product, index: true
  end
end
