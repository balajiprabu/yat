class UpdateColumnFormExecStatusForms < ActiveRecord::Migration
  def change
    change_column :forms, :form_exec_status, :string
  end
end
