class AddPlannedDropDateToProducts < ActiveRecord::Migration
  def change
    add_column :products, :planned_drop_date, :date
    add_column :products, :story_card_validated, :boolean
    add_column :products, :mapped_product, :boolean
  end
end
