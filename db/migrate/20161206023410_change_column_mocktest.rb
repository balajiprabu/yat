class ChangeColumnMocktest < ActiveRecord::Migration
  def change
    change_column :forms, :actual_drop_date, :datetime
  end
end
