class AddUniqueIndexToJoinTable < ActiveRecord::Migration
  def change
    add_index :contracts_forms, [:form_id, :contract_id], unique: true
  end
end
