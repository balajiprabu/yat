class ChangeColumnFilePathTypes < ActiveRecord::Migration
  def change
    change_column :mocktests, :dm_file_path, :text
    change_column :mocktests, :csf_file_path, :text
    change_column :contracts, :dm_file_path, :text
    change_column :contracts, :csf_file_path, :text
  end
end
