class ChangeProducts < ActiveRecord::Migration
  def change
    remove_column :products, :story_card_validated, :boolean
    rename_column :products, :mapped_product, :mapped_or_product_with_zero_forms
  end
end
