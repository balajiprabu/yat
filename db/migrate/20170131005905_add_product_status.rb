class AddProductStatus < ActiveRecord::Migration
  def change
    add_column :products, :product_status, :string, {default: ""}
    change_column_default :products, :mapped_or_product_with_zero_forms, false
    change_column_default :products, :lob, false
  end
end
