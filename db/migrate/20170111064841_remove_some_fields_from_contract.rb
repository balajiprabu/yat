class RemoveSomeFieldsFromContract < ActiveRecord::Migration
  def change
    remove_columns :contracts, :sequence_tested_at, :end_to_end_tested_at
  end
end
