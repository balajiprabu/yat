class MocktestsController < ApplicationController
  before_action :set_mocktest, only: [:show, :edit, :update, :destroy]

  def index
    @mocktests = Mocktest.all
  end

  def show
  end

  def new
    @mocktest = Mocktest.new
  end

  def update
    if @mocktest.update(mock_params)
      redirect_to @mocktest, notice: 'Mocktest was successfully updated.'
    else
      render :edit
    end
  end

  def create
    @mocktest = Mocktest.new(mock_params)
    if @mocktest.save
      redirect_to @mocktest, notice: 'Mocktest was successfully created.'
    else
      render :new
    end
  end

  def edit
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_mocktest
      @mocktest = Mocktest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def mock_params
      params.require(:mocktest).permit(:dm_file_path, :csf_file_path, :form_tested, :tested_at, :allocated_at)
    end
end
