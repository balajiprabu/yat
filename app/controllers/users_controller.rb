class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy,:password_change]

  def new
    @user = User.new
  end

  def index
    @users = User.all
  end

  def password_change
    if @user.update(password_change_params)
      redirect_to @user, notice: 'User password updated successfully'
    else
      render :password_change
    end
  end

  def edit
  end

  def update
    if @user.update(user_update_params)
      redirect_to @user, notice: 'User updated successfully'
    else
      render :edit
    end
  end

  def show
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, notice: "User successfully created"
    else
      render :new
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end
    def user_params
      params.require(:user).permit(:username, :name, :password, :group, :hint)
    end
    def user_update_params
      params.require(:user).permit(:username, :name, :group, :hint)
    end
    def password_change_params
      params.require(:user).permit(:password)
    end
end
