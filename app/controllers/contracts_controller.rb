class ContractsController < ApplicationController
  before_action :set_contract, only: [:show, :edit, :update, :destroy]

  def index
    @contracts = Contract.all
  end

  def show
  end

  def new
    @contract = Contract.new
  end

  def update
    if @contract.update(contract_params)
      redirect_to @contract, notice: 'contract was successfully updated.'
    else
      render :edit
    end
  end

  def create
    @contract = Contract.new(contract_params)
    if @contract.save
      redirect_to @contract, notice: 'contract was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contract
      @contract = Contract.find(params[:id])
    end

    def make_contract_upcase
      contract_params["contract_no"].upcase!
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contract_params
      params.require(:contract).
            permit(:contract_no, :sequence_tested, :end_to_end_tested, :scenario, :allocated_at,
            :dm_file_path, :csf_file_path, :dm_page_range, :contract_weight, :product_id, :user_id)
    end
end
