
class BulkUploader
  def initialize(file_path)
    @file_path = file_path
    @records = []
    @new_product_counter = 0
  end

  def call(output_file_path)

    rows.each_with_index do |row, i|
      next if row.nil? || i.zero?


      p "Row count: #{i}, product: #{row.cells[0].value}" if !row.cells[0].nil?

      build_new_record(row, i)
      # import_records if reached_batch_import_size? || reached_end_of_file?
    end

    if output_file_path.include? "/" and !(output_file_path.end_with? "/")
      output_file_path = output_file_path + "/"
    elsif output_file_path.include? "\\" and !(output_file_path.end_with? "\\")
      output_file_path = output_file_path + "\\"
    end

    output_file_path = output_file_path+"output.xlsx"
    excel.write(output_file_path)

    puts "************* File path : #{output_file_path}*****************"
    return new_product_counter

  end

  private

  attr_reader :file_path, :records
  attr_accessor :new_product_counter

  def excel
    @excel ||= RubyXL::Parser.parse(@file_path)

  end

  def rows
    @rows ||= excel.worksheets[0]

  end

  def row_count
    @row_count ||= rows.count
  end

  def build_new_record(row, i)
    return if row.cells.first.nil?

    product_name = row.cells[0].value
    puts product_name

    planned_drop_date = nil
    if !(row.cells[1].nil?)
      planned_drop_date = row.cells[1].value.to_date
      puts "#{planned_drop_date}, Date: #{planned_drop_date.to_date}"
    end


    mapped = nil
    if !(row.cells[2].nil?)
      mapped = row.cells[2].value
      puts mapped
    end


    lob = nil
    if !(row.cells[3].nil?)
      lob = row.cells[3].value
      puts lob
    end


    release_id = nil
    if !(row.cells[4].nil?)
      release_id = row.cells[4].value
      puts release_id
    end


    rtc_task_updated = nil
    if !(row.cells[5].nil?)
      if row.cells[5].value.in? [1, true, "y", "yes"]
        rtc_task_updated = true
      end
      # rtc_task_updated = row.cells[5].value

      puts rtc_task_updated
    end


    new_product = Product.new(product_name: product_name, planned_drop_date: planned_drop_date,
    mapped_or_product_with_zero_forms: mapped, lob: lob, release_id: release_id, rtc_task_updated: rtc_task_updated)

    if new_product.save
      excel.worksheets[0].insert_cell(i, 7, "Successfully created at: "+new_product.created_at.to_s, formula=nil, :right)
      self.new_product_counter += 1
    else
      excel.worksheets[0].insert_cell(i, 7, new_product.errors.full_messages, formula=nil, :right)
    end

    # check with sasi about how to save file to the path of users specification
    # excel.write("/home/balaji/Desktop/output.xlsx")

  end

  #
  # def import_records
  #   # save multiple records using activerecord-import gem
  #   RecordModel.import(records)
  #
  #   # clear records array
  #   records.clear
  # end

  # def reached_batch_import_size?
  #   (counter % BATCH_IMPORT_SIZE).zero?
  # end
  #
  # def reached_end_of_file?
  #   counter == row_count
  # end
end
