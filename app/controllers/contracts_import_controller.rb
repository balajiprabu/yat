class ContractsImportController < ApplicationController
  def new
    @contract_import = ContractImport.new
  end

  def create
    @contract_import = ContractImport.new(params[:contract_import])
    if @contract_import.save
      redirect_to contracts_path, notice: "Contracts imported successfully.!"
    else
      render :new
    end
  end

end
