require_relative "bulk_uploader"
class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :make_name_upcase, only: [:create, :update]

  def dashboard
    @release = Release.find_by(current: true)
    @products = @release.products.where.not(product_status: "Out of Scope")
  end
  # GET /products
  # GET /products.json
  def index
    @products = Product.all
  end

  def fetch_products_based_on_release
    @products = Release.find(params[:id]).products
  end

  # GET /products/1
  # GET /products/1.json
  def show
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def bulk_upload
  end

  def create_products_in_bulk

    puts params[:file_name]
    obj = BulkUploader.new(params[:file_name])
    records = obj.call(params[:output_file_path])
    redirect_to products_url, notice: "#{records} product(s) created successfully"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    def make_name_upcase
      product_params["product_name"].upcase!
      if !(product_params["lob"].nil?)
        product_params["lob"].upcase!
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).
        permit(:product_name, :planned_drop_date, :mapped_or_product_with_zero_forms,
        :lob, :release_id, :rtc_task_updated)
    end
end
