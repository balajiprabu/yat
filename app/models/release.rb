class Release < ActiveRecord::Base
  validates :name, presence: true, length: {minimum: 7, maximum: 15}, uniqueness: true
  validate :only_one_current_release
  before_validation :set_current_release_and_uncheck_previous_release, :name_to_upcase
  has_many :products

  def only_one_current_release
    if Release.where(current: true).count > 1
      errors.add(:current, " - only one entry can be selected as current release")
    end
  end

  def set_current_release_and_uncheck_previous_release
    if self.current
      Release.where(current: true).where.not(id: id).update_all(current: false)
    else
      if self.id.nil? || !Release.where(id: id)[0].current
      else
        errors.add(:current, " - cannot be unchecked, because there should be atleast 1 entry selected as current release")
      end
    end
    if Release.all.size.zero?
      self.current = true
    end
    true
  end

  def name_to_upcase
    self.name.upcase!
    true
  end
end
