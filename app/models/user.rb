class User < ActiveRecord::Base
  validates :username, presence: true, uniqueness: { case_sensitive: false }, length: { is: 7 }
  validates :name, presence: true, length: { minimum: 3, maximum: 40 }
  `confirmation`
  validates :password, presence: true, length: { minimum: 6, maximum: 20 }, confirmation: {case_sensitive: false}
  # validates :password_confirmation, presence: true
  validates :group, presence: true
end
