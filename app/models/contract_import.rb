class ContractImport
  extend ActiveModel::Naming
  include ActiveModel::Model

  attr_accessor :file, :contracts_hash, :products_hash, :forms_hash, :product_id, :form

  def initialize(attributes = {})
    attributes.each { |name, value| send("#{name}=", value) }
  end

  def persisted?
    false
  end

  def save
    # load_imported_contracts
    # if imported_contracts.map(&:valid?).all?
    #   imported_contracts.each(&:save!)
    #   true
    # else
      imported_contracts.each_with_index do |contract, index|
        contract.errors.full_messages.each do |message|
          errors.add :base, "Row #{index+2}: #{message}"
        end
      end
      true
    #   false
    # end
  end

  def imported_contracts
    @imported_contracts ||= load_imported_contracts
  end

  def load_imported_contracts

    spreadsheet = open_spreadsheet
    header = spreadsheet.row(1)

    p header

    products = Product.select(:id, :product_name)
    @products_hash = Hash[ *products.all.collect { |it| [it.product_name, it.id] }.flatten ]

    p self.products_hash.values

    forms = Form.select(:id, :form_name)
    @forms_hash = Hash[ *forms.all.collect { |it| [it.form_name, it.id] }.flatten ]

    contracts = Contract.select(:id, :contract_no, :scenario, :product_id)
    @contracts_hash = Hash[ *contracts.all.collect { |it| [it.contract_no+it.scenario+it.product_id.to_s, it.id ] }.flatten ]

    row_counter = 1
    spreadsheet.each_row_streaming(offset: 1, pad_cells: true).map do |i|
      column_array=[]
      i.each do |column|
        p column.cell_value
        column_array << column.cell_value
      end

      # logic to change the row 'i' referred here need to change
      row = Hash[[header, column_array].transpose]

      p row

      p "Product_name: #{row["product_name"].upcase}"
      @product_id = find_product_id(row)
      row["product_id"] = self.product_id
      p "Product_id: #{self.product_id}"

      p "Form_name: #{row["form_name"].upcase}"
      @form = find_form(row,forms)
      p "Form: "
      p self.form

      contract = find_or_create_contract(row)
      contract.attributes = row.to_hash.slice(*Contract.contract_params_excel_import)

      p "mapping #{self.form.form_name}, to #{contract.contract_no}"
      if !contract.forms.exists?(self.form.id)
        p "inside: mapping #{self.form.form_name}, to #{contract.contract_no}"
        contract.forms << self.form
        self.form.update(form_status: "Triggered")
      end

      p "Full contract: "
      p contract
      p contract.forms

      if contract.valid?
        contract.save
        new_contract = contract.id
        new_contract_no = contract.contract_no
        new_contract_scenario = contract.scenario
        new_contract_product_id = contract.product_id.to_s
        contracts_hash[new_contract_no+new_contract_scenario+new_contract_product_id] = new_contract
      else
        contract.errors.full_messages.each do |message|
          errors.add :base, "Row #{row_counter}: #{message}"
        end
      end
      row_counter = row_counter+1
      contract

    end
  end

  def find_or_create_contract(row)
    contract_no = row["contract_no"]
    scenario = row["scenario"]
    product_id = row["product_id"]
    if contract_no.nil? || scenario.nil? || product_id.nil?
      return Contract.new
    else
      contract = Contract.find_by(contract_no: contract_no, scenario: scenario, product_id: product_id)
      if contract.nil?
        return Contract.new
      else
        return contract
      end
    end

  end

  def find_form(row, forms)

    form_id = row["form_id"]
    form_name = row["form_name"].upcase
    form = nil
    if form_id.in? forms_hash.values
      form =  Form.find(form_id)
    else
      if form_name.nil? || form_name.blank?
        form = Form.new()
      elsif forms_hash[form_name]
        form = Form.find(forms_hash.fetch(form_name))
      else
        form = Form.new(form_name: form_name)
        if form.save
          form_id = form.id
          form_name = form.form_name
          self.forms_hash[form_name]=form_id
        end
      end
    end
    return form

  end

  def find_product_id(row)

    product_id = row["product_id"]
    p product_id
    product_name = row["product_name"].upcase
    p product_name

    # if product_id is in products_hash.values, return product_id,
    # elsif search product_name in products_hash and return id, if found
    # else try to create product with product_name and add it to products_hash

    if product_id.in? self.products_hash.values
      return product_id
    else
      if product_name.nil?
        return nil
      elsif products_hash[product_name]
        return products_hash.fetch(product_name)
      else
        product = Product.create(product_name: product_name)
        p product
        product_id = product.id
        product_name = product.product_name

        self.products_hash[product_name]=product_id

        return product_id
      end
    end

  end

  def open_spreadsheet
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else raise "Can work only with .csv files, not #{file.original_filename}"
    end
  end

end
