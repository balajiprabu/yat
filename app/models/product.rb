class Product < ActiveRecord::Base
  validates :product_name, presence: true, length: {minimum: 3, maximum: 15}, uniqueness: true
  belongs_to :release
  has_many :forms
  before_validation :upcase_fields

  def upcase_fields
    self.product_name.upcase!
    self.lob.upcase! if !self.lob.nil?
    true
  end
end
