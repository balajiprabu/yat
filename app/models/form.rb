class Form < ActiveRecord::Base
  validates :form_name, presence: true, length: {minimum: 1, maximum: 25}, uniqueness: true
  belongs_to :product
  before_save :fill_actual_drop_date, :fill_executed_at
  before_validation :upcase_fields
  has_and_belongs_to_many :contracts

  def fill_actual_drop_date
    if self.released?
      self.actual_drop_date = DateTime.now()
    end
    true
  end
  def fill_executed_at
    if self.form_exec_status.eql? "Completed"
      self.executed_at = DateTime.now()
    end
      true
  end

  def upcase_fields
    self.form_name.upcase!
    true
  end
end
