class Contract < ActiveRecord::Base
  @@contract_params_excel_import = ["contract_no", "product_id", "scenario"]
  @allowed_types = ["NB", "Reprint", "Reprocess", "Agent Duplicate"]

  validates :contract_no, presence: true, length: {minimum: 3, maximum: 10},
    uniqueness: { scope: [:scenario, :product_id],
      message: "- %{value} already exists. Contract_no, scenario and product combination should be unique" }

  validates :scenario, presence: true, inclusion: { in: @allowed_types,
    message: "%{value} is not a valid scenario, scanario can only be [NB, Reprint, Reprocess, Agent Duplicate]" }

  validates :product_id, presence: {message: "should be present. Note: If this msg is shown in during contract import, we tried to create the product with the product_name, but product could not be created"}


  before_save :populateTimeFields
  belongs_to :product
  has_and_belongs_to_many :forms, before_add: :check_uniqueness

  cattr_accessor :contract_params_excel_import

  def check_uniqueness form
    if forms.exists? form.id
      p "inside uniqueness check - if condition"
      form.errors.add(:base, "this contract-form combination already exists")
      raise ActiveRecord::Rollback
    end
    true
  end

  def changeContractNumberCase
    self.contract_no.upcase!
    true
  end

  def populateTimeFields
    if self.user_id
      self.allocated_at = DateTime.now()
    end

    true
  end

end
