json.extract! release, :id, :name, :current, :created_at, :updated_at
json.url release_url(release, format: :json)