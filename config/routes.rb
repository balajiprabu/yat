Rails.application.routes.draw do

  root 'products#dashboard'
  get 'products/bulk_upload'
  post 'products/bulk_upload' => 'products#create_products_in_bulk'

  get 'contracts_import/new' => 'contracts_import#new', as: :new_contracts_import
  post 'contracts_import/new' => 'contracts_import#create', as: :contracts_import

  resources :releases
  resources :products

  resources :contracts
  resources :forms

  # post 'product/bulk_upload' => 'products#bulk_upload', as: :bulk_upload

  # resources :forms

  # get 'forms' => 'forms#index'
  # post 'forms' => 'forms#create'
  # get 'forms/new' => 'forms#new', as: :new_form
  # get 'forms/:id/edit' => 'forms#edit', as: :edit_form
  # get 'forms/:id' => 'forms#show', as: :form
  # patch 'forms/:id' => 'forms#update'

  # resources :mocktests
  # resources :contracts

  # get 'signup' => 'users#new'
  # resources :users, except: [:new]
  #
  # get 'user/:id/password' => 'users#password_change', as: :password_reset

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'


  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
